import { createStore, combineReducers, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import  profileReducer  from './profileReducer';
import  dialogsReducer  from './dialogsReducer';
import  sidebarReducer  from './sideBarReducer';
import usersReducer from './usersReducer';
import authReducer from "./authReducer";
import thunkMiddleware  from 'redux-thunk';

let reducers = combineReducers( {
    profilePage: profileReducer,
    dialogsPage: dialogsReducer, 
    sidebar: sidebarReducer,
    usersPage: usersReducer,
    auth: authReducer,
});

// let store = createStore(reducers, applyMiddleware(thunkMiddleware), composeWithDevTools());
let store = createStore(reducers, applyMiddleware(thunkMiddleware));

window.store = store;

export default store;