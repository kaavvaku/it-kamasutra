import {profileAPI, userAPI} from './../api/api';

const ADD_POST = 'ADD-POST';
const UPDATE_NEW_POST_TEXT = 'UPDATE-NEW-POST-TEXT';
const SET_USER_PROFILE = 'SET_USER_PROFILE';
const SET_USER_STATUS = 'SET_USER_STATUS';

export const addPostActionCreator = () => ({type: ADD_POST,});
export const updateNewPostTextActionCreator = (text) => ({type: UPDATE_NEW_POST_TEXT, newText: text,});
export const setUserProfile = (profile) => ({type: SET_USER_PROFILE, profile});
export const setUserStatus = (status) => ({type: SET_USER_STATUS, status});
 
let initialState = {
    posts: [
        {id: 1, message: 'Hello, world !!!', likesCount: 4},
        {id: 2, message: 'My first React Component', likesCount: 19},
        {id: 3, message: 'Zero', likesCount: 0},
    ],
    newPostText: '',
    profile: null,
    status: '',
}

const profileReducer = (state = initialState, action) => {
    switch (action.type) {

        case ADD_POST: {
            let newPost = {id: state.posts.length + 1, message: state.newPostText, likesCount: 0,};
            return {...state, posts: [...state.posts, newPost], newPostText: ''};
        }

        case UPDATE_NEW_POST_TEXT: {
            return {...state, newPostText: action.newText};
        }

        case SET_USER_PROFILE: {
            return {...state, profile: action.profile};
        }

        case SET_USER_STATUS: {
            return {...state, status: action.status};
        }

        default:
            return state;
    }
}

export const getUserProfile = (userId) => {
    return (dispatch) => {
        userAPI.getProfile(userId) 
            .then(data => {
                dispatch(setUserProfile(data));
            })
    }
}

export const getUserStatus = (userId) => {
    return (dispatch) => {
        profileAPI.getUserStatus(userId)
            .then(data => {
                dispatch(setUserStatus(data));
            })
    }
}

export default profileReducer;