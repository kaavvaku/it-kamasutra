const UPDATE_NEW_MESSAGE_BODY = 'UPDATE-NEW-MESSAGE-BODY';
const SEND_MESSAGE = 'SEND-MESSAGE';

let initialState = {
  dialogs: [
    { id: 1, name: 'Konstantin'},
    { id: 2, name: 'Irina' },
    { id: 3, name: 'Anna' },
    { id: 4, name: 'Aleksander'  },
        { id: 5, name: 'Tatyana'  },
    { id: 6, name: 'Olga'  }
  ],
  messages: [
    { id: 1, isMyPost: true, message: 'Hi!' },
    { id: 2, isMyPost: false, message: 'Hello!' },
    { id: 3, isMyPost: true, message: 'How are you?' },
    { id: 4, isMyPost: false, message: 'I`m fine! Thanks! And you?' },
    { id: 5, isMyPost: true, message: 'I`m fine too!' },
  ],
  newMessageBody: '',
}

const dialogsReducer = (state = initialState, action) => {
    switch(action.type) {
        case UPDATE_NEW_MESSAGE_BODY: {
          return  {...state, newMessageBody: action.body};
        }

        case SEND_MESSAGE: {
            let body = state.newMessageBody;
            return {
                ...state,
                messages: [...state.messages, {id: 6, isMyPost: true, message: body}],
                newMessageBody: ''
            };
        }
        default:
            return state;
      }
}

export const updateNewMessageBodyCreator = (body) => ( { type: UPDATE_NEW_MESSAGE_BODY, body: body});
export const sendMessageCreator = () => ({ type: SEND_MESSAGE })

export default dialogsReducer;