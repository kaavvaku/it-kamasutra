import {userAPI} from '../api/api';

const FOLLOW = 'FOLLOW';
const UNFOLLOW = 'UNFOLLOW';
const SET_USERS = 'SET_USERS';
const NEXT_PAGE = 'NEXT_PAGE';
const SET_CURRENT_PAGE = 'SET_CURRENT_PAGE';
const SET_TOTAL_USERS_COUNT = 'SET_TOTAL_USERS_COUNT';
const TOGGLE_IS_FETCHING = 'TOGGLE_IS_FETCHING';
const TOGGLE_IS_FOLLOWING_PROGRESS = 'TOGGLE_IS_FOLLOWING_PROGRESS';

let initialState = {
    users: [],
    totalUsersCount: 0,
    pageSize:  10,
    currentPage: 1,
    isFetching: false,
    followingInProgress: [],
}
const usersReducer = (state = initialState, action) => {
    switch (action.type) {
        case FOLLOW:
            return {...state, users: state.users.map ( u => {
                if ( u.id === action.userId ) {
                    return { ...u, followed: true }
                }
                    return u;
                }) }
                
        case UNFOLLOW:
            return { ...state, users: state.users.map ( u => {
                if ( u.id === action.userId) {
                    return { ...u, followed: false}
                }
                    return u;
                }) }

        case SET_USERS:
            return {...state, users: action.users}

        case NEXT_PAGE:
            return {...state, users: [...state.users, ...action.pageUsers] }

        case SET_CURRENT_PAGE:
            return {...state, currentPage: action.currentPage }

        case SET_TOTAL_USERS_COUNT:
            return {...state, totalUsersCount: action.count}

        case TOGGLE_IS_FETCHING:
            return {...state, isFetching: action.isFetching}

        case TOGGLE_IS_FOLLOWING_PROGRESS:
            return { ...state, 
                followingInProgress: action.isFetching  ?  [...state.followingInProgress, action.userId]
                                                        :  state.followingInProgress.filter(id => id !== action.userId)
                }
        default:
            return state;
    }
}

export const followSuccess = (userId) => ({ type: FOLLOW, userId });
export const unfollowSuccess = (userId) => ({ type: UNFOLLOW, userId});
export const setUsers = (users) => ({ type: SET_USERS, users});
export const nextPage = (pageUsers) => ({ type: NEXT_PAGE, pageUsers});
export const setCurrentPage = (currentPage) => ({ type: SET_CURRENT_PAGE, currentPage});
export const setUsersTotalCount = (totalUsersCount) => ({ type: SET_TOTAL_USERS_COUNT, count: totalUsersCount });
export const toggleIsFetching = (isFetching) => ({ type: TOGGLE_IS_FETCHING, isFetching });
export const toggleFollowingProgress = (userId, isFetching) => ({ type: TOGGLE_IS_FOLLOWING_PROGRESS, userId, isFetching});

export const getUsers = (setCurrentPage, pageSize) => {
    return (dispatch) => {
        dispatch(toggleIsFetching(true));
        userAPI.getUsers(setCurrentPage, pageSize)
            .then(data => {
                dispatch(toggleIsFetching(false));
                dispatch(setUsers(data.items));
                (data.totalCount < 100) 
                    ? dispatch(setUsersTotalCount(data.totalCount))  
                    : dispatch(setUsersTotalCount(200))
            })
    }
}

export const follow = ( userId ) => {
    return (dispatch) => {
        dispatch(toggleFollowingProgress(userId, true));
        userAPI.follow(userId)
           .then(data => {
                if (data.resultCode === 0) {
                    dispatch(followSuccess(userId));
                }
                dispatch(toggleFollowingProgress(userId, false));
            })
    }
}

export const unfollow = (userId) => {
    return (dispatch) => {
        dispatch(toggleFollowingProgress(userId, true));
        userAPI.unfollow(userId)
        .then(data => {
                    if (data.resultCode === 0) {
                        dispatch(unfollowSuccess(userId));
                    }
                    dispatch(toggleFollowingProgress(userId, false));
                })
    }
}


export default usersReducer;