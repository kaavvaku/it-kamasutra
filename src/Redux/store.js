import dialogsReducer from './dialogsReducer';
import profileReducer  from './profileReducer';
import sidebarReducer  from './sideBarReducer';

let store = {
  _state: {
    profilePage: {
      posts: [
        { id: 1, message: 'Hello, world !!!'         , likesCount: 4},
        { id: 2, message: 'My first React Component' , likesCount: 19},
        { id: 3, message: 'Zero' , likesCount: 0},
      ],
      newPostText: '',
    },
  
    dialogsPage: {
      dialogs: [
          { id: 1, name: 'Konstantin'},
          { id: 2, name: 'Irina' },
          { id: 3, name: 'Anna' },
          { id: 4, name: 'Aleksander'  },
          { id: 5, name: 'Tatyana'  },
          { id: 6, name: 'Olga'  }
      ],
      messages: [
        { id: 1, isMyPost: true, message: 'Hi!' },
        { id: 2, isMyPost: false, message: 'Hello!' },
        { id: 3, isMyPost: true, message: 'How are you?' },
        { id: 4, isMyPost: false, message: 'I`m fine! Thanks! And you?' },
        { id: 5, isMyPost: true, message: 'I`m fine too!' },
      ],
      newMessageBody: '',
    },
  
    friendsPage: {
      friends: [
        { id: 1, name: 'Alex', age: 35, job: 'manager'},
        { id: 2, name: 'Jim', age: 19, job: 'student'},
        { id: 3, name: 'Bart', age: 25, job: 'developer'},
      ]
    },
  
    sidebar: {
      friends: [
        { id: 1, name: 'Alex'},
        { id: 2, name: 'Jim'},
        { id: 3, name: 'Bart'},
        { id: 4, name: 'Nick'},
      ]
    },
  },
  _callSubscriber() {
    console.log('State changed');
  },

  getState() {
    return this._state;
  },

  subscriber(observer) {
    this._callSubscriber = observer;
  },
 
  addPost() {
    let newPost = {
      id: this._state.profilePage.posts.length+1,
      message: this._state.profilePage.newPostText,
      likesCount: 0,
    };
    this._state.profilePage.posts.push(newPost);
    this._state.profilePage.newPostText = '';
    this._callSubscriber(this._state);
  }, 

  updateNewPostText(newText) {  
    this._state.profilePage.newPostText = newText;
    this._callSubscriber(this._state)
  },

  dispatch(action) {

    this._state.profilePage = profileReducer ( this._state.profilePage, action );
    this._state.dialogsPage = dialogsReducer ( this._state.dialogsPage, action );
    this._state.sidebar = sidebarReducer ( this._state.sidebar, action);
    this._callSubscriber(this._state)

  },
}

window.store = store;

export default store;
