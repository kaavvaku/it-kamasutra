let initialState = {
    friends: [
        { id: 1, name: 'Alex'},
        { id: 2, name: 'Jim'},
        { id: 3, name: 'Bart'},
        { id: 4, name: 'Nick'},
    ]
}

const sidebarReducer = (state = initialState, action) => {
    switch(action.type) {
          
        default:
            return state;
      }
}

export default sidebarReducer;