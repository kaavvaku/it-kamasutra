import React from 'react';
import {connect} from 'react-redux';
import Users from './Users';
import Preloader from "../Common/Preloader/Preloader";
import { 
    nextPage, 
    setCurrentPage, 
    toggleFollowingProgress, 
    getUsers, 
    follow,
    unfollow} from './../../Redux/usersReducer';
import { withAuthRedirect } from '../../hoc/withAuthRedirect';
import { compose } from 'redux';

class UsersContainer extends React.Component {
    componentDidMount() {
        this.props.getUsers(this.props.setCurrentPage, this.props.pageSize);
    }

    onPageChanged = (pageNumber) => {
        this.props.getUsers(pageNumber, this.props.pageSize);
        this.props.setCurrentPage(pageNumber)
    }

    render() {
        return <>
            {this.props.isFetching ? <Preloader/> : null}
            <Users totalUsersCount={this.props.totalUsersCount}
                   pageSize={this.props.pageSize}
                   currentPage={this.props.currentPage}
                   follow={this.props.follow}
                   unfollow={this.props.unfollow}
                   users={this.props.users}
                   isFetching={this.props.isFetching}
                   onPageChanged={this.onPageChanged}
                   isAuth={this.props.isAuth}
                   followingInProgress={this.props.followingInProgress}/>
        </>
    }
}

let mapStateToProps = (state) => {
    return {
        users: state.usersPage.users,
        pageSize: state.usersPage.pageSize,
        totalUsersCount: state.usersPage.totalUsersCount,
        currentPage: state.usersPage.currentPage,
        isFetching: state.usersPage.isFetching,
        isAuth: state.auth.isAuth,
        followingInProgress: state.usersPage.followingInProgress,
    }
}

export default compose(
    connect(mapStateToProps, {nextPage, setCurrentPage, toggleFollowingProgress, getUsers, follow, unfollow }),
    withAuthRedirect
)(UsersContainer);
