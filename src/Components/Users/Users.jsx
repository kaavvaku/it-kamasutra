import styles from "./users.module.css";
import userPhoto from "../../assets/images/no_product.jpg";
import React from "react";
import {NavLink} from "react-router-dom";

let Users = (props) => {
    let pagesCount = Math.ceil(props.totalUsersCount / props.pageSize);
    let pages = [];
    for (let i = 1; i <= pagesCount; i++) {
        pages.push(i)
    }
    return <div>
        <div>{'06 July 2021   12:45   |  last user id: 18137, username: jobangelion'}</div>
        <div>
            {props.totalUsersCount}
        </div>

        <div>
            {pages.map(p => {
                return <span key={p}
                    className={props.currentPage === p ? styles.selectedPage : styles.nonSelectedPage}
                    onClick={(e) => props.onPageChanged(p)}>{p}</span>
            })}
        </div>
        {
            props.users.map(u => <div key={u.id}>
                <div className={styles.divLike}>
                    <div className={styles.divLike}>
                        <hr></hr>
                        <NavLink to={/profile/ + u.id}>
                            {u.photos.small
                                ? <img src={u.photos.small} className={styles.userPhoto} alt={''}/>
                                : <img src={userPhoto} className={styles.userPhoto} alt={''}/>
                            }
                        </NavLink>
                    </div>
                    <div>
                        {u.followed
                            ? <button
                                className={styles.buttonLike}
                                disabled={ !props.isAuth || (props.followingInProgress.some(id => id === u.id)) }
                                onClick={() => {
                                    props.unfollow(u.id);
                                }}>unfollow</button>
                            : <button
                                className={styles.buttonLike}
                                disabled={ !props.isAuth || (props.followingInProgress.some(id => id === u.id)) }
                                onClick={() => {
                                    props.follow(u.id);
                                }}>follow</button>
                        }
                    </div>
                    <div>
                    </div>
                </div>
                <span>
                   
                    <span>
                        <div>{`id: ${u.id}`}</div>
                        <div>{`Name: ${u.name}`}</div>
                        <div>{`Status: ${u.status}`}</div>
                        
                    </span>
                </span>
            </div>)
        }
    </div>
}

export default Users;