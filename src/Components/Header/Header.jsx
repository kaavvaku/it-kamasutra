import React from 'react'
import s from './Header.module.css'
import {NavLink} from "react-router-dom";

const Header = (props) => {
    return (
        <header className={s.header}>
            <img src="https://shb.tomsk.ru/wp-content/uploads/2016/09/Logo200x200-1-150x150.png"></img>
            <div className={s.loginBlock}>
                { props.isAuth  ? <span>{props.login}</span> : <NavLink to={'/login'}>Login</NavLink> }
            </div>

        </header>
    )
}

export default Header;