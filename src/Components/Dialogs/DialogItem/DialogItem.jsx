import React from 'react'
import { NavLink } from 'react-router-dom'
import s from './../Dialogs.module.css'

const DialogItem = (props) => {
    let path = '/dialogs/' + props.id;
    return (
        <div  className={s.dialog + ' ' + s.active}>
            <img className={s.img} src="https://a.deviantart.net/avatars/l/u/luzcl.jpg?1595903958" alt='avatar'></img>
            <NavLink to={path}>{props.name}</NavLink>
        </div>
    )
}

export default DialogItem;