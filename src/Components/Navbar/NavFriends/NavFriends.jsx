import React from 'react'
import './NavFriends.module.css'
import {NavLink} from 'react-router-dom'

const NavFriends = (props) => {
    return (
        <div>
            <NavLink to='/friend' >{props.name}</NavLink>
        </div>
    )
}

export default NavFriends;