import React from 'react';
import s from './Post.module.css';

const Post = (props) => {
    return (
        <div className={s.item}>
            <img src="https://a.deviantart.net/avatars/l/u/luzcl.jpg?1595903958" alt='imag'></img>
            {props.message}
            <div>
                <span>like:</span> {props.likesCount}
            </div>
               
        </div>
    )
}
export default Post;