import React from 'react';
import s from './MyPosts.module.css';
import Post from './Post/Post';

const MyPosts = (props) => {
    let newPostElement = React.createRef();

    let onAddPost = () => { 
        props.addPost();
    }

    let onPostChange = () => {
        let text = newPostElement.current.value;
        props.updateNewPostText(text);
    }
    return (
        <div className={s.postsBlock}>
            <h3>My posts</h3>
            <div >
                <div>
                    <textarea  onChange={onPostChange} ref={newPostElement} value={props.newPostText} />
                </div>
                <div>
                    <button onClick={ onAddPost } >add post</button>
                </div>
            </div>
            
            <div className={s.posts}>
                <div>
                    { props.posts.map( (item) => <Post message={item.message} likesCount={item.likesCount} key={item.id}/> ) }
                </div>
            </div>
        </div>
    )
}

export default MyPosts;