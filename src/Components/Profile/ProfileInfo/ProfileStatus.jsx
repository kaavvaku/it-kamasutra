import React from 'react';
import s from './ProfileInfo.module.css';

class ProfileStatus extends React.Component {
    state = {
        editMode: false,
        // status: 'friend',
    }

    activateEditMode = () => {
        this.setState({
            editMode: true,
        })
    }

    deActivateEditMode = (e) => {
        this.setState({
            editMode: false,
        })
    }

    render() {
        return (
            <div className={s.lastDiv}>
                {!this.state.editMode &&
                <div>
                    <span onDoubleClick={this.activateEditMode}>{this.props.status}</span>
                </div>}
                {this.state.editMode &&
                <div>
                    <input 
                        autoFocus={true} 
                        onBlur={this.deActivateEditMode} 
                        value={this.props.status}/>
                </div>}
            
            </div >
        )
    }
}

export default ProfileStatus;
