import React from 'react';
import s from './ProfileInfo.module.css'
import Preloader from "../../Common/Preloader/Preloader";
import ProfileStatus from './ProfileStatus';
import noFoto from '../../../assets/images/no_foto.jpg';

const ProfileInfo = (props) => {
    if(!props.profile) {
        return <Preloader />
    }
    // debugger;
    return (
        <div>
            <div className={s.descriptionBlock}>
                <img className={s.img} src={props.profile.photos.large ? props.profile.photos.large : noFoto} alt='фото нет'/>
                <ProfileStatus status={props.status}/>
                <div>{`id: ${props.profile.userId}`}</div>
                <div>{'Fullname   :'+props.profile.fullName}</div>
                <div>{'About me   :'+props.profile.aboutMe}</div>
                <div>{'Facebook   :'+props.profile.contacts.facebook}</div>
                <div>{'VK         :'+props.profile.contacts.vk}</div>
                <div>{'Instagram  :'+props.profile.contacts.instagram}</div>
            </div>
        </div>
    )
}

export default ProfileInfo;