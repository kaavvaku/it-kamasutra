// IT-Kamasutra  60 lesson

import './App.css';
import { Route } from 'react-router-dom';
import Navbar from './Components/Navbar/Navbar';
import DialogsContainer from './Components/Dialogs/DialogsContainer';
import LoginPage from './Components/Login/Login';
import Music from './Components/Music/Music';
import Settings from './Components/Settings/Settings';
import UsersContainer from './Components/Users/UsersContainer';
import ProfileContainer from "./Components/Profile/ProfileContainer";
import HeaderContainer from "./Components/Header/HeaderComponent";

const App = (props) => {

  return (
      <div className='app-wrapper'>
        <HeaderContainer />
          <Navbar />

        <div className='app-wrapper-content'>
            <Route path='/dialogs' render={ () => <DialogsContainer />} />
            <Route path='/profile/:userId?' render={ () => <ProfileContainer />} />
            <Route path='/users' render={ () => <UsersContainer /> } />
            <Route path='/login' render={ () => <LoginPage /> } />
            <Route path='/music' render={ () => <Music />} />
            <Route path='/settings' render={ () => <Settings /> } />
        </div>
      </div>
  );
}

export default App;
