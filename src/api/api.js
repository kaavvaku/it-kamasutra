import * as axios from "axios";

const instanсeAxios = axios.create({
    withCredentials: true,
    baseURL: 'https://social-network.samuraijs.com/api/1.0/',
    headers: {
        'API-KEY': '306de14d-c0ed-4443-b14c-8be335883356'
    }
})

export const userAPI = {
    getUsers(setCurrentPage=1, pageSize=10) {
        return instanсeAxios.get(`users?page=${setCurrentPage}&count=${pageSize}`)
            .then(response => {
                return response.data
            })
    },

    follow(userId) {
        return instanсeAxios.post(`follow/${userId}`)
            .then(response => response.data)

    },

    unfollow(userId) {
        return instanсeAxios.delete(`follow/${userId}`)
            .then(response => response.data)
    }, 

    getProfile(userId) {
        console.log('Please use method profileAPI.getProfile(userId)')
        return profileAPI.getProfile(userId)
    }
}

export const profileAPI = {
    getProfile(userId) {
        return instanсeAxios.get(`profile/${userId}`)
            .then(response => response.data )
    },

    getUserStatus(userId) {
        return instanсeAxios.get(`profile/status/${userId}`)
            .then(response => response.data)
    },
}


export const authAPI = {
    me() {
        return instanсeAxios.get(`auth/me`)
            .then(response => response.data)
    }, 

    login(dataObj) {
        return instanсeAxios.post('auth/login')
            .then(response => response)
    }
}


